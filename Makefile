.PHONY: all build run clean format

SRCDIR=src
OUTDIR=out

MAIN_MODULE=Main
TEST_MODULE=Test

GHC_OPTIONS = -threaded -rtsopts -with-rtsopts=-N -odir ${OUTDIR} -hidir ${OUTDIR} -o ${OUTDIR}/main -i${SRCDIR}
# Uncomment this if your system Haskell libraries are dynamically linked - the default on arch
# GHC_OPTIONS+= -dynamic
GHC_OPTIONS+= -Wall -Wno-name-shadowing -Wno-unused-do-bind
GHC_OPTIONS+= -Werror=incomplete-patterns

FIND_SOURCES=find ${SRCDIR} -type f -name '*.*hs'

all: clean test run

build:
	mkdir -p ${OUTDIR}
	ghc -O2 ${GHC_OPTIONS} ${MAIN_MODULE} -main-is ${MAIN_MODULE}.main

build-fast:
	mkdir -p ${OUTDIR}
	ghc ${GHC_OPTIONS} ${MAIN_MODULE} -main-is ${MAIN_MODULE}.main

build-test:
	mkdir -p ${OUTDIR}
	ghc ${GHC_OPTIONS} -o ${OUTDIR}/test ${TEST_MODULE} -main-is ${TEST_MODULE}.main

run: build
	${OUTDIR}/main

run-fast: build-fast build-test
	${OUTDIR}/test
	${OUTDIR}/main

run-watch:
	${FIND_SOURCES} | entr -dcr make run-fast

test: build-test
	${OUTDIR}/test

dev:
	find ${SRCDIR} -type f -name '*.*hs' | entr -dcr make test

clean:
	rm -rf ${OUTDIR}

format:
	find ${SRCDIR} -type f -name '*.*hs' -exec brittany --write-mode=inplace {} ';'
